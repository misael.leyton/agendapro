<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config["path"]["ttw"] = "http://10.120.148.144/ticketwom-test/";
$config["path"]["cli"] = "http://localhost/clinica/";
$config["path"]["prw"] = "http://localhost/prevencion/";
$config["path"]["adm"] = "http://localhost/useradmin/";
$config["path"]["lgc"] = "http://localhost/logincentral/";