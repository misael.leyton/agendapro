<!DOCTYPE html>
<html lang="en">
	<head>
    	
		<meta charset="utf-8">
		<title>AgendaPRO - Agenda de Profesionales Independientes</title>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="">
		<meta name="description" content="">

		<!-- animate css -->
		<link href="<?=base_url()?>assets/css/animate.min.css" rel="stylesheet" type="text/css">
		
		<!-- bootstrap css -->
		<link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<!-- font-awesome -->
		<link href="<?=base_url()?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<!-- google font -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700,800' rel='stylesheet' type='text/css'>

		<!-- custom css -->
		<link href="<?=base_url()?>assets/css/templatemo-style.css" rel="stylesheet" type="text/css">
		

	</head>
	<body>
		<!-- start preloader -->
		<div class="preloader">
			<div class="sk-spinner sk-spinner-rotating-plane"></div>
    	 </div>
		<!-- end preloader -->
		<!-- start navigation -->
		<nav class="navbar navbar-default navbar-fixed-top templatemo-nav" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand">AgendaPRO</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right text-uppercase">
						<li><a href="#home">Home</a></li>
						<li><a href="#feature">Caracteristicas</a></li>
						<!-- <li><a href="#pricing">Pricing</a></li> -->
						<li><a href="#download">Registro</a></li>
						<li><a href="#contact">Contactanos</a></li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- end navigation -->
		<!-- start home -->
		<section id="home">
			<div class="overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10 wow fadeIn" data-wow-delay="0.3s">
							<h1 class="text-upper">Agenda de Profesionales Independientes</h1>
							<p class="tm-white">AgendaPRO es un sofware que integra a diversos profesionales en un mismo sitio, permitiendo a los clientes agendar los distintos servicios que los profesionales ofrecen.</p>
							<img src="<?=base_url()?>assets/images/software-img.png" class="img-responsive" alt="home img">
						</div>
						<div class="col-md-1"></div>
					</div>
				</div>
			</div>
		</section>
		<!-- end home -->
		
		<!-- start feature -->
		<section id="feature">
			<div class="container">
				<div class="row">
					<h2 class="text-uppercase center">Caracteristicas Principales</h2>
					<div class="col-md-5 wow fadeInLeft" data-wow-delay="0.6s">
						<p>AgendaPRO tiene la principal ventaja de agrupar en un solo sitio una gran variedad de servicios con diversas categorias y opciones.</p>
						<p><span><i class="fa fa-mobile"></i></span>Diseño del sistema responsivo y adaptativo, se adapta a cualquier tipo de pantalla.</p>
						<p><i class="fa fa-globe"></i>Disponible desde cualquier lugar</p>
						<p><i class="fa fa-filter"></i>Variedad de Filtros de busqueda</p>
					</div>
					<div class="col-md-7 wow fadeInRight" data-wow-delay="0.6s">
						<img src="<?=base_url()?>assets/images/software-img.png" class="img-responsive" alt="feature img">
					</div>
				</div>
			</div>
		</section>
		<!-- end feature -->

		<!-- start feature1 -->
		<section id="feature1">
			<div class="container">
				<div class="row">
					<div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
						<img src="<?=base_url()?>assets/images/software-img.png" class="img-responsive" alt="feature img">
					</div>
					<div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
						<h2 class="text-uppercase">Mas Caracteristicas</h2>
						<p><i class="fa fa-mobile"></i>Variedad de servicios en diversas ubicaciones</p>
						<p><i class="fa fa-credit-card"></i>Servicio de pago en linea, aumentando la seguridad del pago</p>
						<p><i class="fa fa-sign-in"></i>Registro de usuarios tanto Profesionales como Clientes</p>
					</div>
				</div>
			</div>
		</section>
		<!-- end feature1 -->

		<!-- start download -->
		<section id="download">
			<div class="container">
				<div class="row">
					<div class="col-md-6 wow fadeInLeft" data-wow-delay="0.6s">
						<h2 class="text-uppercase">Registro</h2>
						<p>Registra tu usuario para poder reservar horas o si eres profesional publicar servicios.</p>
						<a href="<?=base_url()?>register" class="btn btn-primary text-uppercase"><i class="fa fa-user-plus"></i> Registrar</a>
					</div>
					<div class="col-md-6 wow fadeInRight" data-wow-delay="0.6s">
						<img src="<?=base_url()?>assets/images/software-img2.png" class="img-responsive" alt="feature img">
					</div>
				</div>
			</div>
		</section>
		<!-- end download -->

		<!-- start contact -->
		<section id="contact">
			<div class="overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
							<h2 class="text-uppercase">Contactanos</h2>
							<p>Si tienes dudas, consultas o acotaciones, no dudes en contactarnos, estamos disponibles para ti </p>
							<address>
								<p><i class="fa fa-map-marker"></i>Av. Libertador Bernardo O'Higgins 2221, Santiago</p>
								<p><i class="fa fa-phone"></i>+56 9 314 60 360</p>
								<p><i class="fa fa-envelope-o"></i> info@agendapro.tk</p>
							</address>
						</div>
						<div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
							<div class="contact-form">
								<form action="#" method="post">
									<div class="col-md-6">
										<input type="text" class="form-control" placeholder="Nombre">
									</div>
									<div class="col-md-6">
										<input type="email" class="form-control" placeholder="Email">
									</div>
									<div class="col-md-12">
										<input type="text" class="form-control" placeholder="Asunto">
									</div>
									<div class="col-md-12">
										<textarea class="form-control" placeholder="Mensaje" rows="4"></textarea>
									</div>
									<div class="col-md-8">
										<input type="submit" class="form-control text-uppercase" value="Enviar">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end contact -->

		<!-- start footer -->
		<footer>
			<div class="container">
				<div class="row">
					<!-- <p>Copyright © 2084 Your Company Name</p> -->
				</div>
			</div>
		</footer>
		<!-- end footer -->
        <script src="<?=base_url()?>assets/js/jquery.js"></script>
        <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/js/wow.min.js"></script>
        <script src="<?=base_url()?>assets/js/jquery.singlePageNav.min.js"></script>
        <script src="<?=base_url()?>assets/js/custom.js"></script>

	</body>
</html>