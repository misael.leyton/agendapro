<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
	function __construct() {
		parent::__construct();

		// Se redirecciona al dashboard por defecto en caso de querer entrar a cualquier metodo (excepto para salir)
		//  para evitar problemas de ejecucion
		// if( $this->session->cntwLogged )
		// 	if( $this->router->fetch_method() != "doLogout" )
		// 		redirect('/dashboard', 'refresh');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	public function register()
	{
		$this->load->view('register');
	}
}
