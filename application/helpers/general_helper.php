<?php
if (!function_exists('noTildesTitle')) {
	/**
	 * [noTildes description]
	 * @param  [type] $toReplace [description]
	 * @return [type]            [description]
	 */
	function noTildesTitle( $toReplace ) {
		$no_permitidas = array (
			"á","é","í","ó","ú","Á","É","Í","Ó","Ú","Ã±","Ã‘","ñ","Ñ", "°"
		);
		$permitidas = array (
			"a","e","i","o","u","A","E","I","O","U","&ntilde;","&Ntilde;","&ntilde;","&Ntilde;", ""
		);
		return str_replace( $no_permitidas, $permitidas ,$toReplace );
	}
}
if (!function_exists('formatRUT')) {
	/**
	 * [formatRUT description]
	 * @param  [type] $rut [description]
	 * @return [type]      [description]
	 */
	function formatRUT( $data ) {
		$data = str_replace( ".", "", trim( $data ) );
		$data = str_replace( "-", "", trim( $data ) );

		$rut = substr( $data, 0, -1 );
		$dv = substr( $data, ( strlen( $data )-1 ) );

		return $rut."-".$dv;
	}
}
if (!function_exists('searchPermiso')) {
	/**
	 * [searchPermiso description]
	 * @param  [type] $search   [description]
	 * @param  [type] $permisos [description]
	 * @return [type]		   [description]
	 */
	function searchPermiso( $search, $permisos = null ) {
		if( !is_null( $permisos ) ) {
			$found = false;
			foreach( $permisos as $key => $value ) {
				if( intval( $key ) == intval( $search ) )
					$found = true;
				if( $found ) break;
			}
			return $found;
		} else
			return false;
	}
}
if (!function_exists('objectToArray')) {
	/**
	 * Funcion convierte un arreglo de "stdclass" a arreglo normal
	 * @author  Jose Vera D.
	 * @version 1.0
	 * @param stdclass Object $d
	 */
	function objectToArray($d) {
		if (is_object($d))
			$d = get_object_vars($d);
		if (is_array($d)) return array_map(__FUNCTION__, $d);
		else return $d;
	}
}
if (!function_exists('arrayToObject')) {
	/**
	 * Funcion convierte un arreglo normal a arreglo "stdclass"
	 * @author  Jose Vera D.
	 * @version 1.0
	 * @param array $d
	 */
	function arrayToObject($d) {
		if (is_array($d)) return (object)array_map(__FUNCTION__, $d);
		else return $d;
	}
}
if (!function_exists('beautifyDate')) {
	/**
	 * Funcion convierte un arreglo normal a arreglo "stdclass"
	 * @author  Jose Vera D.
	 * @version 1.0
	 * @param array $d
	 */
	function beautifyDate( $date ) {
		if( trim( strlen( $date ) ) == 10 )
			$date = date("l d \d\\e F \d\\e\l Y", strtotime($date));
		else 
			$date = date("l d \d\\e F \d\\e\l Y \a \l\a\s H:i", strtotime($date));
		$searchMonth = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		$replaceMonth = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
		// reemplazo mes (manualmente)
		$date = str_replace( $searchMonth, $replaceMonth, $date );

		$searchDay = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
		$replaceDay = array("Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo");
		// reemplazo dia (manualmente)
		$date = str_replace( $searchDay, $replaceDay, $date );

		return $date;
	}
}
if (!function_exists('getTimeLeft')) {
	/**
	 * [getTimeLeft description]
	 * @param  [type] $ini [description]
	 * @param  [type] $act [description]
	 * @param  [type] $rep [description]
	 * @return [type]      [description]
	 */
	function getTimeLeft( $ini, $act, $rep ) {
		$toAdd = $rep * 60 * 60;
		$ini = strtotime( $ini );
		$act = strtotime( $act );
		$restante = ( $ini + $toAdd ) - $act;

		return ($restante<0)?"+":"-";
	}
}
if (!function_exists('calculateRepTime')) {
	/**
	 * [calculateRepTime description]
	 * @param  [type] $ini [description]
	 * @param  [type] $rep [description]
	 * @return [type]      [description]
	 */
	function calculateRepTime( $ini, $rep ) {
		$toAdd = $rep * 60 * 60;
		$ini = strtotime( $ini );

		$finishDate = $ini + $toAdd;

		$dt = new DateTime("@$finishDate");  // convert UNIX timestamp to PHP DateTime
		$dt->setTimezone(new DateTimeZone( date_default_timezone_get() ));

		return $dt->format('Y-m-d H:i:s');
	}
}
if (!function_exists('doSelectedValue')) {
	/**
	 * [doSelectedValue description]
	 * @param  [type] $uf  [description]
	 * @param  [type] $usd [description]
	 * @param  [type] $clp [description]
	 * @return [type]      [description]
	 */
	function doSelectedValue( $uf = null, $usd = null, $clp = null ) {
		if( !is_null( $uf ) ) {
			return $uf." UF";
		} else {
			if( !is_null( $usd ) ) {
				return $usd." USD";
			} else {
				if( !is_null( $clp ) ) {
					return $clp." CLP";
				} else {
					return "--";
				}
			}
		}
	}
}
if (!function_exists('calculateDaysToFinish')) {
	/**
	 * [calculateDaysToFinish description]
	 * @param  [type] $fechaVencimiento [description]
	 * @param  [type] $diasAviso        [description]
	 * @return [type]                   [description]
	 */
	function calculateDaysToFinish( $fechaVencimiento, $diasAviso ) {
		$nowDate = time();
		$realDate = strtotime( $fechaVencimiento );
		$segundos = $diasAviso * 86400;
		$restante = $realDate - $segundos - $nowDate;

		return number_format((float)( ( ( $restante / 60 ) / 60  ) / 24 ), 2, '.', '')." d&iacute;as";
	}
}
if (!function_exists('noTildes')) {
	/**
	 * [noTildes description]
	 * @param  [type] $toReplace [description]
	 * @return [type]            [description]
	 */
	function noTildes( $toReplace ) {
		$no_permitidas = array (
			"á","é","í","ó","ú","Á","É","Í","Ó","Ú","Ã±","Ã‘","ñ","Ñ"
		);
		$permitidas = array (
			"&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;","&ntilde;","&Ntilde;","&ntilde;","&Ntilde;"
		);
		return str_replace( $no_permitidas, $permitidas ,$toReplace );
	}
}
if (!function_exists('htmlPopover')) {
	/**
	 * [htmlPopover description]
	 * @param  [type] $object [description]
	 * @return [type]         [description]
	 */
	function htmlPopover( &$object ) {
		if( !empty( $object ) ) {
			foreach( $object as $key => $value ) {
				$aux = "<ul>";
				$aux .= "<li>";
				switch( intval( $value->estado_pm ) ) {
					case 0:
						$aux .= "Validaci&oacute;n PM <span class='label label-warning'>Pendiente</span>";
						break;
					case 1:
						$aux .= "Validaci&oacute;n PM <span class='label label-success'>Aprobado</span>";
						break;
					case -1:
						$aux .= "Validaci&oacute;n PM <span class='label label-danger'>Rechazado</span>";
						break;
				}
				$aux .= "</li>";
				$aux .= "<li>";
				switch( intval( $value->estado_pr ) ) {
					case 0:
						$aux .= "Validaci&oacute;n PR <span class='label label-warning'>Pendiente</span>";
						break;
					case 1:
						$aux .= "Validaci&oacute;n PR <span class='label label-success'>Aprobado</span>";
						break;
					case -1:
						$aux .= "Validaci&oacute;n PR <span class='label label-danger'>Rechazado</span>";
						break;
				}
				$aux .= "</li>";
				$aux .= "<li>";
				switch( intval( $value->estado_gestor ) ) {
					case 0:
						$aux .= "Validaci&oacute;n Gestor <span class='label label-warning'>Pendiente</span>";
						break;
					case 1:
						$aux .= "Validaci&oacute;n Gestor <span class='label label-success'>Aprobado</span>";
						break;
					case -1:
						$aux .= "Validaci&oacute;n Gestor <span class='label label-danger'>Rechazado</span>";
						break;
					case 2:
						$aux .= "Validaci&oacute;n Gestor <span class='label label-primary'>Gestionando</span>";
						break;
				}
				$aux .= "</li>";
				if ($value->estado == 1) {
					$aux .= "<li>";
					switch($value->ventana_activa) {
						case 'menor':
							$aux .= "Ventana de trabajos <span class='label label-warning'>No Activa</span>";
							break;
						case 'ok':
							$aux .= "Ventana de trabajos <span class='label label-success'>Activa</span>";
							break;
						case 'mayor':
							$aux .= "Ventana de trabajos <span class='label label-danger'>Vencida</span>";
							break;
					}
					$aux .= "</li>";
				}
				$aux .= "</ul>";

				$object[$key]->popover = $aux;
			}
		}
	}
}
?>